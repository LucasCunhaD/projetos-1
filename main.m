clc; clear; close all
format long
SetPaths
% Apenas um teste
fname = 'funcao_objetivo'; % Nome da funcao objetivo
VTR = 1.e-20; % critério de parada utilizado quando conhecemos a solução analítica do problema (não é o seu caso ... não precisa alterar esse valor)
% ============================= LIMITES DAS VARIAVEIS ===============================
       OB = [
			% Asa
            2.00    4.00 		% 01 [m] Corda da raiz
           	0.30    0.50  		% 02 [-] Afilamento
            0.00    0.00       	% 03 [deg] Incidencia
			5.00    25.0		% 04 [deg] Enflechamento
            % EH
            6.00    8.00        % 05 [m] Envergadura
            1.50    2.50		% 06 [m] Corda da raiz
		    0.50    0.80        % 07 [-] Afilamento
		    10.0    30.0        % 08 [deg] Enflechamento
		    0.00	0.00        % 09 [deg] Incidencia (?)
			% EV
			2.00    4.00        % 10 [m] Envergadura
			2.00    2.50        % 11 [m] Corda
			10.0    20.0        % 12 [deg] Enflechamento
			];
% =================================================================================
			 
        XVmin = OB(:,1)';
        XVmax = OB(:,2)';

D = length(OB); % número de variáveis de projeto (problema) 

y=[];           % vetor de parâmetros de entrada
NP = 5*(D - 2);      % tamanho da população
itermax = 70;  % número máximo de gerações/iterações
F = 0.8;        % taxa de perturbação (0-2)
CR = 0.85;      % probabilidade de cruzamento (0-1)
strategy=7;     % estratégia - veja a minha tese para mais detalhes ...
refresh=10;     % nível de impressão na tela - não precisa alterar esse valor ...

[X,FO,NF,POP] = differential_evolution(fname,VTR,D,XVmin,XVmax,y,NP,itermax,F,CR,strategy,refresh);

save('Resultado_Otimizacao.mat','X','FO','NF','POP');
% X é o vetor otimizado
% FO é o valor da função objetivo
% NF é o número de avaliações da função objetivo

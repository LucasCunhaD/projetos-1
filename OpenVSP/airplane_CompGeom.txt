...Comp Geom...
4 Num Comps
6 Total Num Meshes
4080 Total Num Tris

Theo_Area   Wet_Area   Theo_Vol    Wet_Vol  Name
   54.116     46.851      2.440      2.124  Asa            
   11.688     11.562      0.448      0.446  EH             
   84.184     83.462     44.840     44.695  Fuselagem      
    6.409      5.484      0.303      0.261  EV             
-------------------------------------------------
  156.397    147.358     48.032     47.526  Totals         

Tag_Theo_Area   Tag_Wet_Area    Tag_Name
       27.058         23.425    Asa_Surf0      
       27.058         23.425    Asa_Surf1      
        5.844          5.781    EH_Surf0       
        5.844          5.781    EH_Surf1       
       84.184         83.462    Fuselagem_Surf0
        6.409          5.484    EV_Surf0       
WARNING: 2 open meshes merged
     Merged: Asa
     Merged: EH

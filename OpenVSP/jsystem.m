function jsystem(cmd)
%% Platform-specific initialization
if (ispc)
    DEFAULT_SHELL = 'cmd.exe /c';
else
    DEFAULT_SHELL = '/bin/sh -c';
end

%% Handle input
shell = DEFAULT_SHELL;
dir = pwd;
%% Run the command

% Create a java ProcessBuilder instance
pb = java.lang.ProcessBuilder({''});

% Set it's working directory to the current matlab dir
pb.directory(java.io.File(dir));

% Disable stderror redirection to stdout
pb.redirectErrorStream(false);

% If the user doesn't wan't to use a shell, split the command from it's
% arguments. Otherwise, prefix the shell invocation.
shellcmd = [strsplit(shell), cmd];

% Set the command to run
pb.command(shellcmd);

% Start running the new process (non blocking)
process = pb.start();

%% Read output from the process

% Get the result code from the process
process.waitFor();

end
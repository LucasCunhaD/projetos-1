load('Resultado_Otimizacao.mat');

iteracoes = 1:length(POP(1,1,:));
n_pop     = ones(1,length(POP(:,1,1)));
iter      = n_pop'*iteracoes;
%% Corda raiz
C_W(:,:) = POP(:,1,:);
figure
plot(iter,C_W,'xr')
xlabel('Itera��o')
ylabel('Corda da raiz')
grid on

%% Envergadura EV
B_EV(:,:) = POP(:,10,:);
figure
plot(iter,B_EV,'xr')
xlabel('Itera��o')
ylabel('Envergadura EV')
grid on

%% Pontuacao
pont(:,:) = POP(:,13,:);
pont(pont == 0) = NaN;
figure
plot(iter,-pont/1e3,'xr')
xlabel('Itera��o')
ylabel('Alcance [Km]')
grid on
grid minor
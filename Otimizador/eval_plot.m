function f = eval_plot(x);

%% Dados Experimentais
Hmexp = [136.3 108.3 144 43.47];

Fraexp = [9.8115  10.2336   10.7024   20.9698];

%Locais de FRFs (I=For�a Aplicada J=GDL(-1) medido)
I_frf=[2];
J_frf=[4];

% x=[2.505324215758936   2.150706169900739   1.562687199606212   3.010023507150742   2.954286679627525    0.000265970400565   0.000002003263325]*100;


plot = 1;

m0=0.055;
m1=0.039;
m2=0.042;
m3=0.049;
m4=0.046;

k1=x(1);
k2=x(2);
k3=x(3);
k4=x(4);

M=[m0 0 0 0 0
    0 m1 0 0 0
    0 0 m2 0 0
    0 0 0 m3 0
    0 0 0 0 m4];

K=[k1+k2+k3+k4 -k1 -k2 -k3 -k4
    -k1      k1  0   0   0
    -k2      0   k2  0   0
    -k3      0   0   k3  0
    -k4      0   0   0   k4];

%  eq_caract_sym=det(K-lamb*M)
%
%  eq_caract=double(coeffs(eq_caract_sym))
%
%  autovalor=roots(eq_caract)
%
%  w=sqrt(autovalor)

[PHI,B] = eig(K,M);
Fr = zeros(5,1);
for i = 1:length(M)
    Fr(i) = real(sqrt(B(i,i))/(2*pi)); %Fr natural
end
[Fr,I] = sort(Fr); %Ordena as Fr

PHI = PHI(:,I); %Faz com que a matriz modal seja coerente � ordem de Fr
%Matrizes modais
Mm = PHI'*M*PHI;
Km = PHI'*K*PHI;
Cm = x(5)*Mm + x(6)*Km;
%Coeficientes de amortecimento

for i=1:length(M)
    qsi(i)=Cm(i,i)/(2*Fr(i)*Mm(i,i));
end

phi=PHI;
phi(:,1)=phi(:,1)/phi(1,1);
phi(:,2)=phi(:,2)/phi(1,2);
phi(:,3)=phi(:,3)/phi(1,3);
phi(:,4)=phi(:,4)/phi(1,4);
phi(:,5)=phi(:,5)/phi(1,5);


%Frequ�ncia natural amortecida
Fra = Fr'.*sqrt(1-qsi.^2);

%FRF
ome = Fra;
H = zeros(length(M));


frf = 0;
tamanho=size(Hmexp);
n_frf=tamanho(1);

for i = 1:n_frf;
    frf = frf+1;
    
    i=I_frf(frf);
    j=J_frf(frf);
    
    
    for k=2:length(M)
        r = ome./Fra(k);
        H(frf,:) = H (frf,:) + ((2*pi.*ome).^2).*((PHI(i,k)*PHI(j,k)/Km(k,k)).*(1./((1-r.^2)+sqrt(-1).*(2.*qsi(k).*r))));
    end
    Hm(frf,:) = sqrt(real(H(frf,:)).^2+imag(H(frf,:)).^2);
    erro(frf) =  norm((Hmexp(frf,:)  - Hm(frf,2:5))./Hmexp)+std((Hmexp(frf,:) - Hm(frf,2:5))./Hmexp(frf,:))+norm((Fraexp - Fra(2:5))./Fraexp)+std((Fraexp - Fra(2:5))./Fraexp);
    
end

ERRO=sum(erro);
%% Plot
if plot == 1
    %FRF
    ome = linspace(0,100,2000);
    H = 0;

    % for i=1:length(Mm) %Martelo
    %     for j=1:length(Mm) %Aceler�metro

    i = 2; %Martelo na M2
    j = 4; %Aceler�metro na M3
    for k=2:length(M)
    r = ome./Fra(k);
    H = H + ((2*pi.*ome).^2).*((PHI(i,k)*PHI(j,k)/Km(k,k)).*(1./((1-r.^2)+sqrt(-1).*(2.*qsi(k).*r))));
    end
    Hm = sqrt(real(H).^2+imag(H).^2);
    %Gr�fico
    
    open('ref.fig')
    hold on
    
    semilogy(ome,Hm)
    

end
function geo = geometrico_otm(x)
% Essa funcao converte as variaveis de otimizacao na geometria da aeronave
%% Asa
B = 13.8;                       % [m] Envergadura
b1 = B/4;                       % [m] Semi envergadura da 1 secao
b2 = B/4;                       % [m] Semi envergadura da 2 secao
lamb = x(2);
geo.asa.b     = [b1 b2];        % [m] Semi envergadura de cada secao
geo.asa.croot = x(1);           % [m] Corda da raiz
c = [geo.asa.croot 0 geo.asa.croot*lamb];
c(2) = (c(1) + c(3))/2;
geo.asa.lamb  = [c(2)/c(1) c(3)/c(2)];    % [-] Afilamento de cada secao
geo.asa.i     = x(3);           % [graus] Incidencia da asa
geo.asa.sweep = x(4);           % [graus] Angulo de enflechamento da asa (com relacao ao bordo de ataque)
%% Empenagem Horizontal
geo.eh.b      = x(5)/2;         % [m] Semi envergadura da EH
geo.eh.croot  = x(6);           % [m] Corda da raiz
geo.eh.lamb   = x(7);           % [-] Afilamento da EH
geo.eh.sweep  = x(8);           % [graus] Angulo de enflechamento da EH (com relacao ao bordo de ataque)
geo.eh.i      = x(9);           % [graus] Incidencia da EH
geo.eh.xpos   = 7.50;           % [m] Posicao do bordo de ataque com relacao ao bordo de ataque da corda da raiz da asa (+ para tras)
%% Empenagem Vertical
geo.ev.b      = x(10);          % [m] Envergadura da EV
geo.ev.croot  = x(11);          % [m] Corda da raiz da EV
geo.ev.sweep  = x(12);          % [graus] Enflechamento da EV
geo.ev.lamb   = 1;              % [-] Afilamento da EV    
%% Calculo do Alongamento, Area e CMA das superficies
% Asa
nsecoes = length(geo.asa.lamb); % Numero de secoes da asa
geo.asa.S = 0;
geo.asa.CMA = 0;
for i = 1:nsecoes
    % Calculo da area
    Si = (0.5*geo.asa.b(i)*c(i)*(1 + geo.asa.lamb(i)))*2;
    geo.asa.S = geo.asa.S + Si;
    % Calculo da corda media aerodinamica
    ci = (2/3)*c(i)*(1 + geo.asa.lamb(i) + geo.asa.lamb(i)^2)/(1 + geo.asa.lamb(i));
    geo.asa.CMA = geo.asa.CMA + ci*Si;
end
geo.asa.CMA = geo.asa.CMA/geo.asa.S;
geo.asa.B = sum(geo.asa.b)*2;
geo.asa.AR = geo.asa.B^2/geo.asa.S;
% EH
geo.eh.S = (0.5*geo.eh.b*geo.eh.croot*(1 + geo.eh.lamb))*2;
geo.eh.CMA = 2/3*geo.eh.croot*(1 + geo.eh.lamb + geo.eh.lamb^2)/(1 + geo.eh.lamb);
geo.eh.B = geo.eh.b*2;
geo.eh.AR = geo.eh.B^2/geo.eh.S;
% EV
geo.ev.S = 0.5*geo.ev.b*geo.ev.croot*(1 + geo.ev.lamb);
geo.ev.CMA = 2/3*geo.ev.croot*(1 + geo.ev.lamb + geo.ev.lamb^2)/(1 + geo.ev.lamb);
geo.ev.B = geo.ev.b;
geo.ev.AR = geo.ev.B^2/geo.ev.S;
%% Posicao do CG
geo.cg.long = 0.35;             % [-] Posicao longitudinal do CG com relacao a corda da raiz da asa
geo.cg.vert = 0.0;              % [m] Posicao vertical do CG com relacao ao bordo de ataque
end


function comb = combustivel(MTOW,comb_total)
% Funcao para calcular a quantidade de combustivel usada em cruzeiro
% Referencia: Sadraey
comb.total      = comb_total;
comb.subida     = (1 - 0.97*0.98)*MTOW;
comb.descida    = (MTOW - comb.total)/(0.99*0.997) - (MTOW - comb.total);
comb.cruzeiro   = comb.total - comb.descida - comb.subida;
end


function pontuacao = funcao_objetivo(x,~)
tic
% Plot do vetor X
fmt = ['Vetor X: [', repmat('%g ', 1, numel(x)-1), '%g]\n'];
fprintf(fmt, x)
%% Condicoes de analise
% Condicoes de cruzeiro
cruise_mach     = 0.70;                     % [-] Mach de cruzeiro
cruise_alt      = 41000;                    % [ft] Altitude de cruzeiro
cruise_alt      = cruise_alt*0.3048;        % [m] Altitude de cruzeiro
% Condicoes decolagem
dec_alt_min     = 2000;                     % [m] Altitude densidade minima esperada na decolagem
dec_alt_pad     = 1200;                     % [m] Altitude densidade usual (apenas para calculo de estabilidade)
V_dec           = 50;                       % [m/s] Velocidade de decolagem
CL_max          = 2;                        % [-] CL maximo da aeronave
def_prof        = -15;                      % [deg] Deflexao do profundor
aoa_max_trim    = 15;                       % [deg] Angulo maximo que sera feita a trimagem
MTOW_max        = 4.5e3;                    % [Kg] MTOW maximo
PV              = 2.45e3;                   % [Kg] Peso vazio
PPayload		= 750;      				% [Kg] Peso maximo da carga paga (passageiros + bagagem)
frac_comb       = 0.70;                     % [-] Fracao maxima do volume da asa que e ocupada por combustivel
rho_comb        = 0.7775e3;                 % [Kg/m^3] Densidado do combustivel
T_sl            = 45e3;                     % [N] Tracao ao nivel do mar
TSFC            = 73.4/(3600e3);            % [Kg/(N*s)] Consumo de combustivel especifico
g               = 9.81;                     % [m/s^2] Gravidade
S_airfoil       = 0.09;                     % [-] Area por unidade de comprimento do aerofolio
%% Requisitos
Cmalfa_req  = - 1;
Cnb_req     = 0.15;
range_req   = 2200e3;
%% Calculo da geometria a partir do vetor X
geo = geometrico_otm(x);
% Volume da asa
geo.asa.V = geo.asa.S*geo.asa.CMA*S_airfoil;        % [m^3] Volume da asa
%% Calculo peso de combustivel e MTOW
PComb = geo.asa.V*frac_comb*rho_comb;               % [Kg]
if PComb + PPayload + PV > MTOW_max					% Checando se com o peso de combustivel nao ultrapassa o MTOW maximo
	PComb = MTOW_max - (PPayload + PV);
	MTOW = MTOW_max;
else
	MTOW = PComb + PPayload + PV;
end
%% Checando se a area de asa e suficiente
[~,~,~,rho] = atmosisa(dec_alt_min);
CLtrim = MTOW*g/(0.5*rho*V_dec^2*geo.asa.S);
if CLtrim > CL_max
    fprintf('Asa muito pequena, aeronave penalizada\n')
    penal = 1;
else
    penal = 0;
end
%% Criando geometria no OpenVSP
Create_DesFile(geo)
Create_Geometry
%% Calculo trimagem e estabilidade longitudinal
if ~penal
Simetria = 1;
flc.voo = V_dec;
[~,flc.a,~,flc.rho] = atmosisa(dec_alt_pad);
flc.mach = 0.05;
flc.beta = 0;
flc.aoa = [0 aoa_max_trim];
coeffs = RunVSPAero(geo,flc,Simetria,def_prof);
Cmalfa = (diff(coeffs(1).coeffs.CMy))/(diff(flc.aoa)*pi/180);
if coeffs(1).coeffs(1).CMy(2) < 0
    penal = 1;
    fprintf('Aeronave nao trimavel\n')
elseif Cmalfa > Cmalfa_req
    penal = 1;
    fprintf('Aeronave instavel longitudinalmente\n')
end
end
%% Calculo estabilidade direcional
if ~penal
flc.aoa = 0;
flc.beta = [0 5];
Simetria = 0;
coeffs = RunVSPAero(geo,flc,Simetria);
Cnb = -(coeffs(1).coeffs(2).CMz - coeffs(1).coeffs(1).CMz)/(diff(flc.beta)*pi/180);
if Cnb < Cnb_req
    penal = 1;
    fprintf('Aeronave instavel direcionalmente\n')
end
end
%% Calculo do alcance em cruzeiro e alcance especifico
if ~penal
Simetria    = 1; % Se simetria = 1, o arrasto da EV sera desprezado!
flc.beta    = 0;
flc.mach    = cruise_mach;
[flc.T,flc.a,~,flc.rho] = atmosisa(cruise_alt);
flc.voo     = flc.a*flc.mach;
flc.aoa     = 0:4;
coeffs      = RunVSPAero(geo,flc,Simetria);
[P] = polyfit(coeffs.coeffs.CL.^2,coeffs.coeffs.CDtot,1); % Fazendo uma regressao linear para achar CDo e k (CDtot = CDo + kCL^2)
k = P(1);
CDo = P(2);
ct = TSFC*g;	% [1/s]
comb = combustivel(MTOW,PComb);
Wini = (MTOW - comb.subida)*g;
Wfin = (MTOW - comb.subida - comb.cruzeiro)*g;
A = flc.rho*flc.voo^2*geo.asa.S*sqrt(CDo);
Range = (flc.voo)/(ct*sqrt(k*CDo))*(atan((2*sqrt(k)*Wini)/(A)) - atan((2*sqrt(k)*Wfin)/(A))); % [m]
% General Aircraft Design: Applied Methods And Procedures | Cap. 20.2 - pag. 899
% Altitude e velocidade constantes
end
if ~penal
    pontuacao = -Range;
    fprintf('Tempo: %.1f segundos\nRange: %.0f Km\n',toc,Range/1000);
    fprintf('Area de asa: %.1f\n\n',geo.asa.S);
else
    pontuacao = 0;
    fprintf('Tempo: %.1f segundos\n\n',toc);
end
end
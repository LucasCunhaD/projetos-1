function coeffs = GetCoeffs(filename,flc)
%% Essa funcao pega os coeficientes gerados pelo vsp no arquivo .history
% (gerei a maior parte dessa funcao automaticamente pelo Matlab)

%% Initialize variables.
delimiter = ' ';
startRow = 3;
endRow = inf;

%% Format for each line of text:
formatSpec = '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
textscan(fileID, '%[^\n\r]', startRow(1)-1, 'WhiteSpace', '', 'ReturnOnError', false);
dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true, 'TextType', 'string', 'ReturnOnError', false, 'EndOfLine', '\r\n');
for block=2:length(startRow)
    frewind(fileID);
    textscan(fileID, '%[^\n\r]', startRow(block)-1, 'WhiteSpace', '', 'ReturnOnError', false);
    dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true, 'TextType', 'string', 'ReturnOnError', false, 'EndOfLine', '\r\n');
    for col=1:length(dataArray)
        dataArray{col} = [dataArray{col};dataArrayBlock{col}];
    end
end

%% Close the text file.
fclose(fileID);

%% Essa foi a parte que eu mexi
% Nessa parte eu apenas coloco os coeficientes numa struct pra acessa-los
% mais facilmente

AviaoDegenGeom = [dataArray{1:end-1}]; % Essa variavel contem todas as linhas do arquivo .history em formato string
k = 0;
n_mach = length(flc.mach);
n_aoa  = length(flc.aoa);
n_beta = length(flc.beta);
aux = zeros(n_mach*n_aoa*n_beta,17);    % Prealocando essa variavel
for i = 1:length(AviaoDegenGeom) % Nesse loop eu pego apenas as linhas que tem os coeficientes e transformo para double
    if AviaoDegenGeom(i,1) == 'Iter' %#ok<*BDSCA>
        k = k+1;
        aux(k,:) = str2double(AviaoDegenGeom(i+1,2:end));
    end
end
for i = 1:n_mach % Nesse loop eu so coloco os coeficientes numa struct para ficar mais organizado e facil de mexer
    coeffs(i).mach = flc.mach(i);
    for j = 1:n_beta
       coeffs(i).coeffs(j).beta = flc.beta(j);
       idx = (aux(:,3) == flc.beta(j)) & (aux(:,1) == flc.mach(i)); % Essa variavel contem os indices dos valores de mesmo mach e beta
       coeffs(i).coeffs(j).aoa = flc.aoa;
       % Pegando os coeffs
       coeffs(i).coeffs(j).CL       = aux(idx,4);
       coeffs(i).coeffs(j).CDo      = aux(idx,5);
       coeffs(i).coeffs(j).CDi      = aux(idx,6);
       coeffs(i).coeffs(j).CDtot    = aux(idx,7);
       coeffs(i).coeffs(j).LD       = aux(idx,9);
       coeffs(i).coeffs(j).CFx      = aux(idx,11);
       coeffs(i).coeffs(j).CFy      = aux(idx,12);
       coeffs(i).coeffs(j).CFz      = aux(idx,13);
       coeffs(i).coeffs(j).CMx      = aux(idx,14);
       coeffs(i).coeffs(j).CMy      = aux(idx,15);
       coeffs(i).coeffs(j).CMz      = aux(idx,16);
    end
end

function coeffs = RunVSPAero(geo,flc,Symmetry,control)
% Essa funcao roda o VSPAero para o calculo dos coeficientes aerodinamicos
% geo: struct que contem a geometria da aeronave
% flc: struct com as condicoes da analise
% Symmetry: simetria no eixo XZ
% control: deflexao do profundor (opcional) positivo para baixo
cd('OpenVSP')   % Entrando na pasta em que esta o openVSP
wakeiters = 0;
%% Checando se ha simetria (roda + rapido com simetria)
if Symmetry
    Simetria = 'Y';
else
    Simetria = 'NO';
end
%% Checando se sera rodado com profundor defletido
if nargin == 3 || control == 0
    control = 0;
end
%% Criando card com as condicoes de analise
fname = 'Aviao_DegenGeom.vspaero'; % Nome do arquivo
ID = fopen(fname,'w');
fprintf(ID,'Sref = %f\n',geo.asa.S);
fprintf(ID,'Cref = %f\n',geo.asa.CMA);
fprintf(ID,'Bref = %f\n',geo.asa.B);
fprintf(ID,'X_cg = %f\n',geo.asa.croot*geo.cg.long);
fprintf(ID,'Y_cg = %f\n',0);
fprintf(ID,'Z_cg = %f\n',geo.cg.vert);
% Mach
fprintf(ID,'Mach = ');
if length(flc.mach) > 1
    fprintf(ID,'%f, ',flc.mach(1:end-1));
    fprintf(ID,'%f\n',flc.mach(end));
else
   fprintf(ID,'%f\n',flc.mach);
end
% AoA
fprintf(ID,'AoA = ');
if length(flc.aoa) > 1
    fprintf(ID,'%f, ',flc.aoa(1:end-1));
    fprintf(ID,'%f\n',flc.aoa(end));
else
   fprintf(ID,'%f\n',flc.aoa);
end
% Beta
fprintf(ID,'Beta = ');
if length(flc.beta) > 1
    fprintf(ID,'%f, ',flc.beta(1:end-1));
    fprintf(ID,'%f\n',flc.beta(end));
else
   fprintf(ID,'%f\n',flc.beta);
end
fprintf(ID,'Vinf = %f\n',flc.voo);
fprintf(ID,'Rho = %f\n',flc.rho);
fprintf(ID,'ReCref = %f\n',1e+7);
fprintf(ID,'ClMax = %f\n',-1);
fprintf(ID,'MaxTurningAngle = %f\n',-1);
fprintf(ID,'Symmetry = %s\n',Simetria);
fprintf(ID,'FarDist = %f\n',-1);
fprintf(ID,'NumWakeNodes = %d\n',0);
fprintf(ID,'WakeIters = %d\n',wakeiters);
if ~control
    fprintf(ID,'NumberOfControlGroups = 0\n');
else
    fprintf(ID,'NumberOfControlGroups = 1\n');
    fprintf(ID,'Profundor\n');
    fprintf(ID,'EH_Surf0_Profundor,EH_Surf1_Profundor\n');
    fprintf(ID,'1, -1\n');        % Ganho na deflexao dos flaps
                                  % (para fazer um flap simetrico, e necessario colocar com sinal trocado)
    fprintf(ID,'%f\n',control);   % Deflexao do profundor
end

fprintf(ID,'Preconditioner = Matrix\n');
fprintf(ID,'Karman-Tsien Correction = N\n');
fprintf(ID,'Stability Type = 0');
fclose(ID);
[~,~] = dos('vspaero -omp N -geom Aviao'); % Isso e necessario
[~,~] = dos('vspaero -omp N Aviao_DegenGeom');
cd ..
coeffs = GetCoeffs('Aviao_DegenGeom.history',flc);
end


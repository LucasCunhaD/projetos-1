clc; clear
% Estou usando esse codigo apenas para testes
%% Geometria da aeronave
% Asa
geo.asa.sweep = 30;         % [graus] Angulo de enflechamento da asa (com relacao ao bordo de ataque)
geo.asa.b     = [4.5 4.5];  % [m] Semi envergadura de cada secao
geo.asa.croot = 2;          % [m] Corda da raiz
geo.asa.lamb  = [sqrt(0.5) sqrt(0.5)];  % [-] Afilamento de cada secao
geo.asa.i     = 2;          % [graus] Incidencia da asa
% Empenagem Horizontal
geo.eh.sweep  = 30;         % [graus] Angulo de enflechamento da EH (com relacao ao bordo de ataque)
geo.eh.b      = 4;          % [m] Semi envergadura da EH
geo.eh.i      = -2;         % [graus] Incidencia da EH
geo.eh.lamb   = 0.5;        % [-] Afilamento da EH
geo.eh.croot  = 2.0;        % [m] Corda da raiz
geo.eh.xpos   = 9.0;        % [m] Posicao do bordo de ataque com relacao ao bordo de ataque da corda da raiz da asa (+ para tras)
% Empenagem Vertical
geo.ev.sweep  = 30;         % [graus] Enflechamento da EV
geo.ev.b      = 2;          % [m] Envergadura da EV
geo.ev.croot  = 1.5;        % [m] Corda da raiz da EV
% Posicao do CG
geo.cg.long = 0.4;          % [-] Posicao longitudinal do CG com relacao a corda da raiz da asa
geo.cg.vert = 0.0;          % [m] Posicao vertical do CG com relacao ao bordo de ataque
%% Calculos geometricos
geo = Geometrico(geo);
%% Criar geometria
Create_DesFile(geo)         % Criacao do arquivo .des
Create_Geometry(1)             % Computando geometria contida no .des
%% Condicoes de analise
flc.mach = 0.2;                             % [-] Mach
flc.beta = [0 1 2 3];                       % [graus] Beta
flc.aoa = 5:6;                              % [graus] Alpha
flc.h  = 1000;                              % [m] Altitude densidade
[~,flc.a,~,flc.rho] = atmosisa(flc.h);      % Calculo da velocidade do som e densidade do ar
flc.voo = flc.a*flc.mach;                   % [m/s] Velocidade
%% Rodando o openVSP
coeffs = RunVSPAero(geo,flc);
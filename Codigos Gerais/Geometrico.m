function geo = Geometrico(geo)
% Essa funcao calcula algumas caracteristicas geometricas, como a area das
% superficies, alongamento e corda media
%% Asa
nsecoes = length(geo.asa.lamb); % Numero de secoes da asa
geo.asa.S = 0;
geo.asa.CMA = 0;
c = [geo.asa.croot geo.asa.croot*geo.asa.lamb(1)];
for i = 1:nsecoes
    % Calculo da area
    Si = (0.5*geo.asa.b(i)*c(i)*(1 + geo.asa.lamb(i)))*2;
    geo.asa.S = geo.asa.S + Si;
    % Calculo da corda media aerodinamica
    ci = (2/3)*c(i)*(1 + geo.asa.lamb(i) + geo.asa.lamb(i)^2)/(1 + geo.asa.lamb(i));
    geo.asa.CMA = geo.asa.CMA + ci*Si;
end
geo.asa.CMA = geo.asa.CMA/geo.asa.S;
geo.asa.B = sum(geo.asa.b)*2;
geo.asa.AR = geo.asa.B^2/geo.asa.S;
%% EH
geo.eh.S = (0.5*geo.eh.b*geo.eh.croot*(1 + geo.eh.lamb))*2;
geo.eh.CMA = 2/3*geo.eh.croot*(1 + geo.eh.lamb + geo.eh.lamb^2)/(1 + geo.eh.lamb);
geo.eh.B = geo.eh.b*2;
geo.eh.AR = geo.eh.B^2/geo.eh.S;
%% EV
geo.ev.lamb = 1;
geo.ev.S = 0.5*geo.ev.b*geo.ev.croot*(1 + geo.ev.lamb);
geo.ev.CMA = 2/3*geo.ev.croot*(1 + geo.ev.lamb + geo.ev.lamb^2)/(1 + geo.ev.lamb);
geo.ev.B = geo.ev.b;
geo.ev.AR = geo.ev.B^2/geo.ev.S;
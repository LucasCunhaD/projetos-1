function Create_Geometry(~)
% Essa funcao roda o VSP e computa a geometria
cd('OpenVSP')   % Entrando na pasta do vsp
if nargin       % nargin e o numero de inputs que a funcao recebeu
    [~,~] = system('vsp -script CreateGeometry_Slow.vspscript');  % Caso a funcao receba algum input e rodado esse script (ele cria o arquivo .vsp3 que pode ser usado pra ver a geometria da aeroanve, porem + demorado)
else
    [~,~] = dos('vsp -script CreateGeometry_Fast.vspscript');  % Caso a funcao nao receba input e rodado esse outro script (esse cria apenas o arquivo .csv para calcular os coeficientes + rapido pra otimizacao)
end
cd ..           % Voltando para a pasta inicial
function Create_DesFile(geo)
% Essa funcao cria o arquivo .des
cd('OpenVSP')           % Entrando na pasta em que esta o openVSP (isso e necessario pq o arquivo tem q estar na msm pasta do vsp.exe)
fname = 'design.des';   % Nome do arquivo
ID = fopen(fname,'w');  % Criando o arquivo .des e dando permissao pra editar
parnum = 16;            % Numero de parametros
fprintf(ID,'%d\n',parnum);
% Asa
corda_ponta1 = geo.asa.croot*geo.asa.lamb(1);                               % Corda da ponta da 1 secao
corda_ponta2 = geo.asa.croot*geo.asa.lamb(1)*geo.asa.lamb(2);               % Corda da ponta da 2 secao
fprintf(ID,'ENSTYYTXWYC:Asa:XSec_0:Twist: %f\n',geo.asa.i);                 % Incidencia
fprintf(ID,'RIAQZOBQUVO:Asa:XSec_2:Span: %f\n',geo.asa.b(2));               % Semi envergadura da 2 secao
fprintf(ID,'RYVRKKKPUEN:Asa:XSec_2:Tip_Chord: %f\n',corda_ponta2);          % Corda da ponta 2 secao
fprintf(ID,'KIIYQAIKHRG:Asa:XSec_1:Root_Chord: %f\n',geo.asa.croot);        % Corda da raiz
fprintf(ID,'IHBYOKAXVBI:Asa:XSec_1:Span: %f\n',geo.asa.b(1));               % Semi envergadura da secao 1
fprintf(ID,'BWLMLHVPIOT:Asa:XSec_1:Sweep: %f\n',geo.asa.sweep);             % Enflechamento
fprintf(ID,'YULLPLMCLPF:Asa:XSec_1:Tip_Chord: %f\n',corda_ponta1);          % Corda da ponta da 1 secao
% EH
fprintf(ID,'CYSEQBLLSVD:EH:XSec_1:Root_Chord: %f\n',geo.eh.croot);          % Corda da raiz
fprintf(ID,'KRRZZSFWKZM:EH:XSec_1:Span: %f\n',geo.eh.b);                    % Semi-envergadura
fprintf(ID,'IWUPVGOHSNN:EH:XSec_1:Sweep: %f\n',geo.eh.sweep);               % Enflechamento
fprintf(ID,'LHBGYTQNJGU:EH:XSec_1:Taper: %f\n',geo.eh.lamb);                % Afilamento
fprintf(ID,'MJNFANFQTOZ:EH:XSec_0:Twist: %f\n',geo.eh.i);                   % Incidencia
fprintf(ID,'PPSUGCGJWUG:EH:XForm:X_Rel_Location: %f\n',geo.eh.xpos);        % Posicao longitudinal
% EV
fprintf(ID,'TLTTCXQJBZB:EV:XSec_1:Root_Chord: %f\n',geo.ev.croot);          % Corda
fprintf(ID,'YCDFHBLHTEN:EV:XSec_1:Span: %f\n',geo.ev.b);                    % Envergadura
fprintf(ID,'DUBRMMGVRWQ:EV:XSec_1:Sweep: %f',geo.ev.sweep);                 % Enflechamento
fclose(ID);
cd ..                                                                       % Voltando para a pasta inicial